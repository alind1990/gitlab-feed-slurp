# GitLab Feed Slurp
Quick and dirty util to grab the activity feed from GitLab and convert it to HTML.

Use by [my web site](https://gitlab.com/rubidium-dev/www).